The .sty files are generated using the following sources

- data/

  This directory contains:

    - pixelart pictures, in a human-readable but awfully inefficient format: 1s are black pixels, 0s are white pixels;
    - calculator keys, as a yaml file.

- bin/

   Binaries `generate.keys` and `generate.pixelart` are used to process data files, and to generate .sty files expected by graph35.sty.
