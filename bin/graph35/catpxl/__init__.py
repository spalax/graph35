# Copyright Louis Paternault 2018
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Module to display pixelart files to the terminal.

A pixel art file contains only 0, 1 and line breaks.
"""

class CatpxlError(Exception):
    """Generic error that is to be nicely displayed."""
    pass

ASCIIART = {
    "00": " ",
    "01": "▄",
    "11": "█",
    "10": "▀",
    }

def catpxl(image):
    """Print a pixalart image using ASCIIART."""
    lines = image.split()
    if len(lines) % 2 == 1:
        lines.append("0"*len(lines[0]))
    for y in range(0, len(lines), 2): # pylint: disable=invalid-name
        for x in range(len(lines[y])): # pylint: disable=invalid-name
            try:
                print(ASCIIART[lines[y][x]+lines[y+1][x]], end="")
            except KeyError:
                if lines[y][x] not in "01":
                    wrongchar = lines[y][x]
                else:
                    wrongchar = lines[y+1][x]
                raise CatpxlError(f"Character '{wrongchar}' is not allowed.")
        print()
