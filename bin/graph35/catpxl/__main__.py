#!/usr/bin/env python3

# Copyright Louis Paternault 2018
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Display pixel art files to the terminal."""

import sys

from . import catpxl, CatpxlError

if __name__ == "__main__":
    for filename in sys.argv[1:]:
        try:
            with open(filename) as file:
                catpxl(file.read())
        except (OSError, CatpxlError) as error:
            sys.stderr.write(f"Error in file '{filename}': {error}\n")
            continue
