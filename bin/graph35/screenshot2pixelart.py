#!/usr/bin/env python3

# Copyright Louis Paternault 2018
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Extract pixelarts from a screenshot."""

import argparse
import glob
import os

from PIL import Image

from . import PIXELARTDIR
from .catpxl import catpxl

class PixelartDict:
    """Dictonary matching pixelart names to their content."""

    def __init__(self, family):
        self.family = family

        self._dict = {}
        for filename in glob.iglob(os.path.join(
                self.directory, "*.pxl"
            )):
            with open(filename) as file:
                self._dict[os.path.basename(filename[:-4])] = file.read().strip()

    @property
    def directory(self):
        """Return current directory."""
        return os.path.join(PIXELARTDIR, self.family)

    def has_pixelart(self, pixelart):
        """Return True if pixelart is already known."""
        return pixelart in self.values()

    def values(self):
        """Iterate over dictionary values."""
        return self._dict.values()

    def add(self, pixelart):
        """Maybe add pixelart.

        - Check if it is known.
        - Ask user for its name.
        - Eventually save it (if unknown and named).
        """
        catpxl(pixelart)
        while True:
            name = input("Name ? (empty to ignore) ")
            if not name:
                print("Ignored…")
                break
            if name in self:
                print(f"Name '{name}' already used by the following pixelart.")
                catpxl(self[name])
                continue
            self.save(name, pixelart)
            return

    def save(self, name, pixelart):
        """Write pixelarts to files."""
        with open(os.path.join(self.directory, f'{name}.pxl'), mode="w") as file:
            file.write(pixelart)
        self[name] = pixelart

    def __iter__(self):
        yield from self._dict

    def __getitem__(self, key):
        return self._dict[key]

    def __setitem__(self, key, value):
        self._dict[key] = value

def extract_pixelart(image, position, size):
    """Extract a single pixelart from the image."""
    pixelart = ""
    for y in range(size[1]):
        for x in range(size[0]):
            if sum(image.getpixel((position[0]+x, position[1]+y))) > 128 * 3:
                pixelart += "0"
            else:
                pixelart += "1"
        pixelart += "\n"
    return pixelart.strip()

def extract_pixelart_function(filename):
    """Extract `function` pixelarts."""
    try:
        with Image.open(filename).convert("RGB") as image:
            if image.size != (132, 68):
                print(
                    "Image size of '{}' is invalid: is {}; should be {}."
                    .format(filename, image.size, (132, 68))
                    )
                return
            for offset in range(6):
                yield extract_pixelart(
                    image,
                    (4 + 21*offset, 58),
                    (19, 8),
                    ).strip()
    except FileNotFoundError:
        print(f"Ignoring file '{filename}': no such file or directory.")
        return

def extract_pixelart_menuchar(filename):
    """Extract `menuchar` pixelarts."""
    try:
        with Image.open(filename).convert("RGB") as image:
            if image.size != (132, 68):
                print(
                    "Image size of '{}' is invalid: is {}; should be {}."
                    .format(filename, image.size, (132, 68))
                    )
                return
            for xoffset in range(4):
                for yoffset in range(3):
                    yield extract_pixelart(
                        image,
                        (26 + 30*xoffset, 22 + 19*yoffset),
                        (5, 5),
                        ).strip()
    except FileNotFoundError:
        print(f"Ignoring file '{filename}': no such file or directory.")
        return

def extract_pixelart_menu(filename):
    """Extract `menu` pixelarts."""
    try:
        with Image.open(filename).convert("RGB") as image:
            if image.size != (132, 68):
                print(
                    "Image size of '{}' is invalid: is {}; should be {}."
                    .format(filename, image.size, (132, 68))
                    )
                return
            for xoffset in range(4):
                for yoffset in range(3):
                    pixelart = [list(line) for line in extract_pixelart(
                        image,
                        (2 + 30*xoffset, 9 + 19*yoffset),
                        (30, 19),
                        ).strip().split("\n")]
                    for x in range(5):
                        for y in range(5):
                            pixelart[y+13][x+24] = "0"
                    yield "\n".join("".join(line) for line in pixelart)
    except FileNotFoundError:
        print(f"Ignoring file '{filename}': no such file or directory.")
        return

EXTRACTORS = {
    'function': extract_pixelart_function,
    'menuchar': extract_pixelart_menuchar,
    'menu': extract_pixelart_menu,
    }

def commandline_parser():
    """Return command line parser."""
    parser = argparse.ArgumentParser(
        description='Extract pixel art from screenshots.',
        )

    parser.add_argument(
        'family',
        choices=EXTRACTORS.keys(),
        help='Family of pixel art to extract.',
        )

    parser.add_argument(
        'files',
        nargs='+',
        help='Screenshots to process (as file names of images).',
        )

    return parser

def main():
    """Main function."""
    options = commandline_parser().parse_args()
    pixelartdict = PixelartDict(options.family)
    for filename in options.files:
        print(f"Analysing file {filename}…")
        for pixelart in EXTRACTORS[options.family](filename):
            if pixelartdict.has_pixelart(pixelart):
                print("Ignoring known pixelart.")
            else:
                pixelartdict.add(pixelart)

if __name__ == "__main__":
    main()
